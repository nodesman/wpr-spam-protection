<?php
$dir = dirname(__FILE__);
include "$dir/packer/jp.php";

function _wpr_generateRandomString($length)
{
	$string="";
	for ($iter=0;$iter<$length;$iter++)
	{
		$whichOne = rand(0,2);
		
		if ($whichOne == 1)
		{
			$randomNumber = rand(65,90);
		}
		else if ($whichOne ==2)
		{
			$randomNumber = rand(97,122);
		}
		else
		{
			$randomNumber = rand(48,57);
		}
		$character = chr($randomNumber);
		$string .= $character;
	}
	return $string;
}

function _wpr_captch_question(&$question,&$answer)
{
	$whichOperation = rand(0,3);
	
	switch ($whichOperation)
	{
		case 0:
		     $oper1 = rand(1,20);
			 $oper2 = rand(1,20);
			 $question = array($oper1,"+",$oper2);
			 $answer= $oper1+$oper2;
		
		break;
		
		case 1:
			$oper1 = rand(8,20);
			$oper2 = rand(1,8);
			$question = array($oper1,'-',$oper2);
			$answer = $oper1-$oper2;		
		break;
		
		case 2:
			$oper1 = rand(1,9);
			$oper2 = rand(1,9);
			$answer = $oper1*$oper2;
			$question = array($oper1,'*',$oper2);
		break;		
		case 3:
			$oper2 = rand(2,9);
			$answer = rand(1,9);
			$oper1 = $answer*$oper2;
			$question = array($oper1,'/',$oper2);
		break;
		
	}	
}

ob_start();
$question = array();
$answer ="";
_wpr_captch_question($question,$answer);

$nameOfFormField = _wpr_generateRandomString(10);
$keyValue = _wpr_generateRandomString(15);
$cookieName = _wpr_generateRandomString(8);
$timeWhenExpires = time()+1800;
setcookie($cookieName,$keyValue,$timeWhenExpires);
_wpr_addCaptchaRecord($keyValue,$answer,$timeWhenExpires,$nameOfFormField);
$string = $question[0]." ".$question[1]." ".$question[2];

$stringLength = strlen($string);
?>
var ol_tls_dtd = new Array();
<?php
for ($iter=0;$iter<$stringLength;$iter++)
{
	$char = @$string[$iter];
	$value = ord($char);
	?>
ol_tls_dtd[<?php echo $iter; ?>] = <?php echo $value; ?>;    
<?php
}
?>
//name of the form field 
<?php
$lengthOfFormFieldName = strlen($nameOfFormField);
for ($iter=0;$iter<$lengthOfFormFieldName;$iter++)
{
	$numbers[] = ord($nameOfFormField[$iter]);
}
$fieldName = implode(",",$numbers);
?>
var hlt_32_zl42 = [<?php echo $fieldName ?>];
var hl3="";
for (var i in hlt_32_zl42)
{
   hl3 += String.fromCharCode(hlt_32_zl42[i]);
}
document.write("<tr><td>");
for (var iter in ol_tls_dtd)
{
    document.write(String.fromCharCode(ol_tls_dtd[iter]));
}
document.write("  is </td><td>");
document.write('<input type="text" size="2" name="'+hl3+'" value="" />');
document.write("</td></tr>");
<?php
$script = ob_get_clean();
$packer = new JavaScriptPacker($script, 'Normal', true, false);
echo $packer->pack();
?>