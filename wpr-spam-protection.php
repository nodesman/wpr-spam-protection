<?php
/*
Plugin Name: WP Autoresponder Spam Protection
Plugin URI: http://www.wpresponder.com
Description: Plugin to add spam protection to WP Autoresponder blogs.
Version: 0.1
Author: Raj Sekharan
Author URI: http://www.nodesman.com/
*/

$dir = dirname(__FILE__);
include "$dir/caplib.php";
add_action("_wpr_subscriptionform_form_field","_wprsp_form_field",1,1);
function _wprsp_form_field($form_id) {

    $whetherSelected=false;
    if (!empty($form_id)) {
        $forms_with_captcha = get_option("_wprsp_forms_with_captcha");
        if (in_array(intval($form_id),$forms_with_captcha))
            $whetherSelected=true;


    }

    ?>
<tr>
    <td><strong>Whether include spam protection field</strong></td>
    <td><input type="checkbox" value="1" name="wprfs_spam_enable" <?php if ($whetherSelected) { ?>checked="checked" <?php } ?>/> Include spam protection CAPTCHA field.</td>
</tr>
<?php
}

function _wprsp_form_saver($form_id)  {
    $options = get_option("_wprsp_forms_with_captcha");
    if (isset($_POST['wprfs_spam_enable'])) {
        $options[] = $form_id;
        $options = array_unique($options);
    }
    else
        {
        $options = array_diff($options, array($form_id));
    }
    update_option("_wprsp_forms_with_captcha",$options);
}


add_action("_wpr_subscriptionform_created_handler_save","_wprsp_form_saver",1,1);
add_action("_wpr_subscriptionform_edit_handler_save","_wprsp_form_saver",1,1);
add_action("_wpr_subscriptionform_code","_wprsp_captcha_field",1,1);


function _wprsp_captcha_field($form_id) {

    $whetherSelected=false;
    if (!empty($form_id)) {
        $forms_with_captcha = get_option("_wprsp_forms_with_captcha");
        if (in_array(intval($form_id),$forms_with_captcha))
            $whetherSelected=true;
    }
    if ($whetherSelected) {
?>
<script src="<?php echo get_bloginfo("home"); ?>/?iota=1" type="text/javascript">
</script>
<?php
    }

}

add_action("init","_wprsp_init");

function _wprsp_init() {

    if (isset($_GET['iota'])) {
        include "osho.php";
        exit;
    }
}


function _wprfs_verify_captcha() {
    
      //captcha check.
        $fid = (int) $_POST['fid'];
        if ($fid)
        {
            $captcha_fields = get_option("_wprsp_forms_with_captcha");

            if (in_array($fid,$captcha_fields))
                    {
            if (!_wpr_verifyCaptchaEntry())
            {
                    error("Captcha verification failed");
            }
           }
        }
        else
        {
                header("HTTP/1.1 404 Not Found");
                exit;
        }
}


add_action("_wpr_subscriptionform_prevalidate","_wprfs_verify_captcha");