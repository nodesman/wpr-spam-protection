<?php
/*----------------CAPTCHA Record Related Functions Start Here-------------------------*/
function _wpr_addCaptchaRecord($key,$answer,$expires,$formFieldName)
{
	$captcha_fields = getCaptchaArray();
	if (!is_array($captcha_fields))
	{
		$captcha_fields = array();
	}
	$record = array($answer,$formFieldName,$expires);
	$captcha_fields[$key] = $record;
	
	delete_option("_wpr_captchas");
	add_option("_wpr_captchas",$captcha_fields);
}

function isWprCaptchaCookie($key)
{
	$captchaArray = getCaptchaArray();
	$keys = array_keys($captchaArray);
	return (in_array($key,$keys))?1:0;
}

function getCaptchaArray()
{
	$captchaArray = get_option("_wpr_captchas");
	return $captchaArray;
}

function _wpr_deleteExpiredCaptchaRecords()
{
	$captcha_fields = getCaptchaArray();
	foreach ($captcha_fields as $key=>$field)
	{
		if ($field[2] > time())
		{
			unset($captcha_fields[$key]);
		}
	}
}

function _wpr_getEntireCaptchaData()
{
	return getCaptchaArray();
}

function _wpr_getCaptchaRecord($key)
{
	$captcha_fields = getCaptchaArray();
	return $captcha_fields[$key];
}

function _wpr_deleteCaptchaRecord($key)
{
	$captcha_fields = getCaptchaArray();
	unset($captcha_fields[$key]);	
	delete_option("_wpr_captchas");
	add_option("_wpr_captchas",$captcha_fields);
}


function _wpr_verifyCaptchaEntry()
{
	if (count($_COOKIE) > 0 )
	{
		foreach ($_COOKIE as $CookieName=>$key)
		{

			if (isWprCaptchaCookie($key))
			{
				$values = 	_wpr_getCaptchaRecord($key);
				if (isset($_POST[$values[1]]))
				{

					if ($values[2] < time())
					{//expired!
						_wpr_deleteExpiredCaptchaRecords();
						return false;
					}				
					$answerSent = (int) $_POST[$values[1]];
					if ($values[0]== $answerSent)
					{

						return true;
					}
					else
					{
						return false;
					}
				}
				else
					continue;
			}
						
		}
		
		return false;
	}
	else 
		return false; // bot!!!
}


